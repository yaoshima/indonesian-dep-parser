
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
// require('./tree.js');
require('./js-treex-view.min.js');



function get_conllu_format(deps, tokens, tags){
	deps.sort((a,b)=>{
		if (a[1] < b[1]) {
	        return -1;
	    }
	    if (b[1] > a[1]) {
	        return 1;
	    }
	    return 0;
	})

	let conllu_text = ""

	let sent = ""
	let annotated_tokens = ""

	for(let i=0; i<deps.length; i++){
		sent += tokens[i+1] + " "

		// "1\tNamun\tnamun\tSCONJ\tS--\t_\t6\tmark\t_\t_\n"
		annotated_tokens +=   `${deps[i][1]}\t${tokens[i+1]}\t_\t${tags[i+1]}\t_\t_\t${deps[i][0]}\t${deps[i][2]}\t_\t_\n`

	}

	conllu_text += "# text = " + sent.trim() + "\n"
	conllu_text += annotated_tokens
	conllu_text += '\n'

	return conllu_text


}

function showTree(output_file_content) {
  let output_file_tree;
  if (!output_file_content) return;
  if (output_file_tree) return;

  var trees = [];
  var tree_desc = [];
  var tree_nodes = [];

  var lines = output_file_content.split(/\r?\n/);
  lines.push('');
  for (var i in lines) {
    if (lines[i].match(/^(#|\d+-)/)) continue;
    if (lines[i]) {
      var parts = lines[i].split('\t');
      for (var i in parts) if (parts[i] == "_") parts[i] = "";

      if (tree_desc.length) tree_desc.push([' ', 'space']);
      tree_desc.push([parts[1], 'w'+parts[0]]);

      if (!tree_nodes.length) tree_nodes.push({id:'w0', ord:0, parent:null, data:{id:"0",form:"<root>"}, labels:['<root>','','']});
      tree_nodes.push({id:'w'+parts[0], ord:tree_nodes.length, parent:parts[6]!==""?'w'+parts[6]:null, data:{
        id:parts[0], form:parts[1], lemma:parts[2], upostag:parts[3], xpostag:parts[4],
        feats:parts[5], head:parts[6], deprel:parts[7], deps:parts[8], misc:parts[9]
      }, labels:[parts[1], '#{#00008b}'+parts[7], '#{#004048}'+parts[3]]});
    } else if (tree_nodes.length) {
      var last_child = [];
      for (var i = 1; i < tree_nodes.length; i++) {
        var head = tree_nodes[i].data.head!=="" ? parseInt(tree_nodes[i].data.head) : "";
        if (head !== "") {
          if (!last_child[head]) tree_nodes[head].firstson = 'w'+i;
          else tree_nodes[last_child[head]].rbrother = 'w'+i;
          last_child[head] = i;
        }
      }

      trees.push({desc:tree_desc,zones:{conllu:{trees:{"a":{layer:"a",nodes:tree_nodes}}}}});
      tree_desc = [];
      tree_nodes = [];
    }
  }

  output_file_tree = trees;
  // setTimeout(function() { $('#tree').treexView(trees); }, 0);
  $('#tree').treexView(trees);
  $('[class^=_]').css('min-height',0)
  $('[sentence]').remove()
}



window.Vue = require('vue');
window.displaCy = require('./displacy.js');



import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

Vue.use(Loading);




new Vue({
	el : '#app',
	mounted() {
		
	},
	methods : {
		getParseTree(){
			let loader = this.$loading.show({
				loader: 'dots',
				color: 'gray'
			})

			axios.post('http://127.0.0.1:8080/predict',{
				'sent' : this.rawSent
			})
			.then((response) => {
				let resp = response.data
				// console.log(response.data)
				// console.log(resp.num_dep[0].transition)

				this.transitions =  resp.num_dep[0].transition

				let words = resp.tags.map(function(val, i){
					return { tag: val, text: resp.tokens[i]  }
				})

				let arcs = resp.num_dep[0].dependencies.map(function(val){
					let head = val[0]
					let dep = val[1]
					let label = val[2]

					if (head < dep){
						return { dir: 'right', start: head, end: dep, label: label }
					}
					else{
						return { dir: 'left', start: dep, end: head, label: label }
					}
				})

				console.log(arcs)


				let displacy = new displaCy('http://localhost:8080', {
				  container: '#displacy',
				  format: 'spacy',
				  distance: 120
				})

				const parse = {
				    arcs: arcs,
				    words: words
				};

				displacy.render(parse, {
				    color: '#000000'
				});

				this.hideHeader = false


				// show tree
				console.log(resp)
				let deps = resp.num_dep[0].dependencies
				let tags = resp.tags
				let tokens = resp.tokens


				let conllu = get_conllu_format(deps, tokens, tags)
				showTree(conllu)


				loader.hide()

				

			})
			
		}
	},
	data : {
		rawSent : null,
		transitions: [],
		hideHeader : true
	}
})








