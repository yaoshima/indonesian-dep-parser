<!DOCTYPE html>
<html>
<head>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Test</title>
	<link rel="stylesheet" href="/css/app.css">
</head>
<body class="bg-gray-200">
	
	<div id="app">	
		<div class="flex">
			<div class="w-1/3 bg-teal-100 min-h-screen">
				<!-- <h1 class="text-center my-2 text-lg font-semibold">Indonesian Dependency Parser</h1> -->
				<form class="mx-4 my-4 flex flex-col p-6 rounded-lg">	
					<div class="text-sm">Please enter a sentence to be parsed:</div>			
					<hr class="divider mt-1 mb-3">
					<textarea class="border p-1 rounded-lg" rows="6" cols="50" v-model="rawSent"></textarea>
					<div>
						<!-- <button type="button" @click="getParseTree">Kirim</button> -->
						<button class="mt-2 btn-eleveted full-btn" type="button" @click="getParseTree" >Parse</button>
					</div>			
				</form>
			</div>

			<div class="flex flex-1 flex-col justify-center">
				<p v-if="hideHeader" class="text-center text-3xl font-bold">No Result Yet!</p>
				<div class="mx-6 flex bg-white rounded-lg my-4 shadow-md" v-bind:class="{ 'p-2': !hideHeader }">
					<div class="flex-1">
						<p v-if="!hideHeader" class="font-bold my-2">Dependency graph: </p>

						<div id="displacy" style="font-size: 12px">
						</div>
					</div>
					
					<div style="width: 200px">
						<p  v-if="!hideHeader" class="font-bold my-2">Transition sequence: </p>

						<ul class="text-sm list-decimal list-inside font-mono">
						  <li v-for="transition in transitions">
						    @{{ transition }}
						  </li>
						</ul>
					</div>
				</div>

				<div class="mx-6 flex bg-white rounded-lg my-4 shadow-md" v-bind:class="{ 'p-2': !hideHeader }">
					<div class="flex-1">
						<p v-if="!hideHeader" class="font-bold my-2">Dependency tree: </p>

						<div id="tree">
						</div>
					</div>
					
					<div style="width: 200px">
						<p  v-if="!hideHeader" class="font-bold my-2">Transition sequence: </p>

						<ul class="text-sm list-decimal list-inside font-mono">
						  <li v-for="transition in transitions">
						    @{{ transition }}
						  </li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		

		

		<!-- <div id="displacy">
		</div> -->
	</div>



	
	<script src="/js/app.js"></script>
</body>
</html>